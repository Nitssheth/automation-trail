
Feature: Test Inward Module

  Background: User is logged in
    Given User is on login page
    When User enter valid username and password and click on login button
    Then User should able to login successfully and navigate to Dashboard page
    Then User Allow the Push Notification Access for all


  Scenario: User verify Dashboard screen and redirected to Inward
    When User verify the dashboard page by clicking on cards
    And User verify redirection to inward
    Then User should redirected to Inward

  Scenario: User verify inward list filters and search
    When User verify redirection to inward
    And User verify the filters in inward list
    And User verify the search feature in inward list
    Then User should able to search in inward list

  Scenario: User try to create inward
    When User verify redirection to inward
    Then User should redirected to Inward
    When User try to create new inward
    Then User should able to create new inward


  Scenario: User try to edit inward
    When User verify redirection to inward
    Then User should redirected to Inward
    When User try to edit the inward
    Then User should able to edit the inward

  Scenario: User try to delete inward
    When User verify redirection to inward
    Then User should redirected to Inward
    When User try to delete the inward
    Then User should able to delete the inward

  Scenario: User try to outward the inward
    When User verify redirection to inward
    Then User should redirected to Inward
    When User try to outward the inward
    Then User should able to outward the inward

  Scenario: User try to print the inward
    When User verify redirection to inward
    Then User should redirected to Inward
    When User try to print the inward
    Then User should able to print the inward

  Scenario: User try to To_Repairer the inward
    When User verify redirection to inward
    Then User should redirected to Inward
    When User try to To_Repairer the inward
    Then User should able to To_Repairer the inward


  Scenario: User try to From_Repairer the inward
    When User verify redirection to inward
    Then User should redirected to Inward
    When User try to From_Repairer the inward
    Then User should able to From_Repairer the inward