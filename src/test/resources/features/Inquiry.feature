
Feature: Test Inquiry Module

  Background: User is logged in
    Given User is on login page
    When User enter valid username and password and click on login button
    Then User should able to login successfully and navigate to Dashboard page
    Then User Allow the Push Notification Access for all


  Scenario: User Verify the dashboard screen and redirected to inquiry
    When User verify the dashboard page by clicking on cards
    Then User should redirected to inquiry module

  Scenario: User Verify inquiry screens and filters
    When User verify the dashboard page by clicking on cards
    Then User verify the inquiry filters and list export

  Scenario: User try to create an inquiry
    When User verify the dashboard page by clicking on cards
    Then User should redirected to inquiry module
    When User try to create new Inquiry
    Then User Should able to create new Inquiry

  Scenario: User try to Edit an inquiry
    When User verify the dashboard page by clicking on cards
    Then User should redirected to inquiry module
    When User try to create new Inquiry
    Then User Should able to create new Inquiry
    When User check status is "Pending" or "Paused,In-Progress" in edit inquiry
    Then Status DropDown field should have "Paused" or "Pending,In-Progress,Completed,Paused"
    When User try to Edit the inquiry for "Pending,In-Progress" or "Paused" in inquiry
    Then User Verify the updated inquiry and redirected to list screen

  Scenario: User try to Delete an inquiry
    When User verify the dashboard page by clicking on cards
    Then User should redirected to inquiry module
    When User try to delete the inquiry
    Then User Should able to delete the inquiry

