Feature: Test Login functionality of web application


  Scenario: User can login with valid username and password
    Given User is on login page
    When User enter valid username and password and click on login button
    Then User should able to login successfully and navigate to Dashboard page

  Scenario: User should not login with invalid username and password
    Given User is on login page
    When User enter Invalid username and password and click on login button
    Then User should not able to login successfully and Redirected to login page

  Scenario: User should not login with blank username and password
    Given User is on login page
    When User enter blank username and password and click on login button
    Then User should not able to login successfully and Redirected to login page

  Scenario: User should not login with space in username and password
    Given User is on login page
    When User enter space in username and password and click on login button
    Then User should not able to login successfully and Redirected to login page

  Scenario: Enable Push Notification of User
    Given User is on login page
    When User Allow the Push Notification Access
    Then User should not able to view the notification popup


  Scenario: User should not reset password using invalid email
    Given User is on login page
    When User enter Invalid email and try to reset password
    Then User should not able to login successfully and Redirected to login page

  Scenario: User can reset password using valid email
    Given User is on login page
    When User enter valid email and resetting new password
    Then User should not able to login successfully and Redirected to login page

  Scenario: User can logout successfully
    Given User is on login page
    When User enter valid username and password and click on login button
    Then User should able to login successfully and navigate to Dashboard page
    When User click on logout and redirected to login page
    Then User Should able to logout successfully and redirected to login page