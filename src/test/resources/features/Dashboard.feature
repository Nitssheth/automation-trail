Feature: Test Dashboard screen Verification

  Background: User is logged in
    Given User is on login page
    When User enter valid username and password and click on login button
    Then User should able to login successfully and navigate to Dashboard page
    Then User Allow the Push Notification Access for all

  Scenario: Check Dashboard screen using cards
    When User should able to login successfully and navigate to Dashboard page
    Then User verify the dashboard page by clicking on cards


  Scenario Outline: Check Dashboard screen and redirected to particular modules
    When User verify the dashboard page by clicking on cards
    Then User should redirected to module by clicking card "<Modules>"
    Examples:
      | Modules     |
      | Inquiry     |
      | InOutward   |
      | ServiceCall |
      |             |


  Scenario Outline: Check Dashboard with side menu redirection to module
    When User verify the dashboard page by clicking on cards
    Then User should redirected to particular module through "<SideMenu>" and redirected to the particular page
    Examples:
      | SideMenu        |
      | Dashboard       |
      | CallCoordinator |
      | Inquiry         |
      | InOutward       |
      | ServiceCall     |
      | Quotation       |
      | Customer        |
      | Master          |
      | Admin           |
