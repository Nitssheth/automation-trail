Feature: Test Inward Module

  Background: User is logged in
    Given User is on login page
    When User enter valid username and password and click on login button
    Then User should able to login successfully and navigate to Dashboard page
    Then User Allow the Push Notification Access for all


  Scenario:User verify Dashboard screen and redirected to ServiceCall
    When User verify the dashboard page by clicking on cards
    And User verify redirection to ServiceCall
    Then User should redirected to ServiceCall

  Scenario: User verify ServiceCall list filters and search
    When User verify redirection to ServiceCall
    And User verify the filters in ServiceCall list
    And User verify the search feature in ServiceCall list
    Then User should able to search in ServiceCall list


  Scenario: User try to create ServiceCall
    When User verify redirection to ServiceCall
    Then User should redirected to ServiceCall
    When User try to create new ServiceCall
    Then User should able to create new ServiceCall

  Scenario: User try to edit ServiceCall
    When User verify redirection to ServiceCall
    Then User should redirected to ServiceCall
    When User try to edit the ServiceCall
    Then User should able to edit the ServiceCall

  Scenario: User try to close ServiceCall
    When User verify redirection to ServiceCall
    Then User should redirected to ServiceCall
    When User try to close the ServiceCall
    Then User should able to close the ServiceCall

  Scenario: User try to delete ServiceCall
    When User verify redirection to ServiceCall
    Then User should redirected to ServiceCall
    When User try to delete the ServiceCall
    Then User should able to delete the ServiceCall

    Scenario: User try to print ServiceCall
      When User verify redirection to ServiceCall
      Then User should redirected to ServiceCall
      When User try to print the ServiceCall
      Then User should able to print the ServiceCall

  Scenario: User try to view ServiceCall
    When User verify redirection to ServiceCall
    Then User should redirected to ServiceCall
    When User try to view the ServiceCall
    Then User should able to view the ServiceCall

  Scenario: User Verify ServiceCall screens and filters
    When User verify the serviceCall list export
    Then User should able to export the serviceCall