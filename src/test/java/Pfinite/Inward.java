package Ateroid;

import Ateroid_Org.Pages.InwardPage;
import Ateroid_Org.Utils;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Iterator;
import java.util.Set;

public class Inward extends Utils {

    InwardPage pageObj = new InwardPage();


    @Then("User verify redirection to inward")
    public void redirectionToInwardList() {
        pageObj.inwardList();
    }

    @When("User should redirected to Inward")
    public void verifyInwardList() {
        String Login_URL = "https://sample.pfinite.com/inwards";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Redirection failed to Inward", URL, Login_URL);
    }

    @When("User verify the filters in inward list")
    public void inwardListFilters(){
        pageObj.inwardFilters();
    }

    @When("User verify the search feature in inward list")
    public void inwardListSearch(){
    pageObj.inwardSearch("Test");
    }

    @Then("User should able to search in inward list")
    public void verifyInwardSearch(){
    WebElement label = driver.findElement(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[5]/div/div[1]/div[2]/form/div/input"));
    String Value = label.getAttribute("value");
    System.out.println(Value);
    String searchLabel = "Test";
    Assert.assertEquals("Search failed",searchLabel,Value);
    }

    @When("User try to create new inward")
    public void createInward(){
        pageObj.createInward();
    }

    @Then("User should able to create new inward")
    public void verifyInward(){
        String Login_URL = "https://sample.pfinite.com/inwards";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Inward fail to create", URL, Login_URL);
    }

    @When("User try to edit the inward")
    public void editInward(){
        pageObj.inwardOptions("Edit");
    }

    @Then("User should able to edit the inward")
    public void verifyEditInward() throws InterruptedException {
        By actualMessage = By.cssSelector("div > div.toast-message");
        String message = "Save Succeeded !";
        Assert.assertEquals("Inward updated successfully.",message,get_Text(actualMessage));
    }


    @When("User try to delete the inward")
    public void deleteInward(){
        pageObj.inwardOptions("Delete");
    }

    @Then("User should able to delete the inward")
    public void verifyDeleteInward(){
        By actualMessage = By.cssSelector("div > div.toast-message");
        String message = "Successfully deleted";
        Assert.assertEquals("Inward deleted successfully.",message,get_Text(actualMessage));
    }

    @When("User try to outward the inward")
    public void outward(){
        pageObj.inwardOptions("Outward");
    }

    @Then("User should able to outward the inward")
    public void verifyOutward() throws InterruptedException {
        Thread.sleep(3);
        String Login_URL = "https://sample.pfinite.com/inwards";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Outward failed", URL, Login_URL);
    }

    @Then("User try to print the inward")
    public void printInward() {
        pageObj.inwardOptions("Print");
    }

    @Then("User should able to print the inward")
    public void verifyPrint() {
        String mainWindowHandle = driver.getWindowHandle();
        Set<String> allWindowHandles = driver.getWindowHandles();
        Iterator<String> iterator = allWindowHandles.iterator();
        while (iterator.hasNext()) {
            String ChildWindow = iterator.next();
            driver.switchTo().window(ChildWindow);
            String URls = driver.getCurrentUrl();
            if (!mainWindowHandle.equalsIgnoreCase(ChildWindow) && URls.contains("https://sample.pfinite.com/print-inward")) {
                System.out.println("Print success");
                break;
            }
        }
    }

    @When("User try to view the inward")
    public void viewInward() {
        pageObj.inwardOptions(" ");
    }

    @Then("User should able to view the inward")
    public void verifyViewInward() {
        boolean view = false;
        String URL = driver.getCurrentUrl();
        if (URL.contains("edit?action=all&active_tab=inward")) {
            view = true;
            System.out.println("View success");
        } else {
            System.out.println("View failed");
        }
    }

    @When("User try to To_Repairer the inward")
    public void toRepairerInward() {
        pageObj.inwardOptions("To Repairer");
    }

    @Then("User should able to To_Repairer the inward")
    public void verifyToRepairerInward() {
        String Login_URL = "https://sample.pfinite.com/inwards";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("to repairer failed", URL, Login_URL);
    }

    @When("User try to From_Repairer the inward")
    public void fromRepairerInward() {
        pageObj.inwardOptions("From Repairer");
    }

    @Then("User should able to From_Repairer the inward")
    public void verifyFromRepairerInward() throws InterruptedException {
        Thread.sleep(20);
        String Login_URL = "https://sample.pfinite.com/inwards/";
        String URL = driver.getCurrentUrl();
        if (URL.contains(Login_URL)) {
            System.out.println("From Repairer success");
        } else {
            System.out.println("From Repairer not verified");
        }
    }
}
