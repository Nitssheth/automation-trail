package Ateroid;

import Ateroid_Org.LoadProps;
import Ateroid_Org.Utils;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class Login extends Utils {

    Ateroid_Org.Pages.LoginPage loginPagePages = new Ateroid_Org.Pages.LoginPage();

    @When("User Allow the Push Notification Access")
    public static void EnablePushNotifications() throws InterruptedException {
        Ateroid_Org.Pages.LoginPage loginPagePages = new Ateroid_Org.Pages.LoginPage();
        loginPagePages.enterLoginDetails("admin", "admin");
        sleep(10);
        List<WebElement> l = driver.findElements(By.xpath("//*[@id=\"onesignal-slidedown-allow-button\"]"));
        if (l.size() == 0) {
            System.out.println("Element not present");
        } else {
            loginPagePages.EnableWebPushNotification();
        }
    }

    @Then("User should not able to view the notification popup")
    public static void PopupVerification() {
        Ateroid_Org.Pages.LoginPage loginPagePages = new Ateroid_Org.Pages.LoginPage();
        List<WebElement> l = driver.findElements(By.xpath("//*[@id=\"onesignal-slidedown-allow-button\"]"));
        if (l.size() == 0) {
            System.out.println("Push notification allowed successfully.");
        } else {
            System.out.println("Push notification popup is not present");
        }

    }

    @Given("User enter valid email and resetting new password")
    public static void ForgotPasswordLinkValidCredentials() throws InterruptedException {

        Ateroid_Org.Pages.LoginPage loginPagePages = new Ateroid_Org.Pages.LoginPage();
        loginPagePages.ForgotPassword("nishitshethinfo@gmail.com");
        String URL = driver.getCurrentUrl();
        By actualMsg = By.cssSelector(".toast-message");
        String errorMsg = "OTP sent to registered Email address.";
        Assert.assertEquals("Invalid credentials", errorMsg, get_Text(actualMsg));
        sleep(10);
        loginPagePages.EnterOTPAndChangePassword();
        Assert.assertEquals("not navigate to login url", loginPagePages.successPassChangeUrl, driver.getCurrentUrl());
    }

    @When("User enter blank username and password and click on login button")
    public static void LoginWithBlankCredentials() {
        String Login_URL = "https://sample.pfinite.com/auth/login";
        Ateroid_Org.Pages.LoginPage loginPage = new Ateroid_Org.Pages.LoginPage();
        loginPage.enterLoginDetails("", "");
        System.out.println(driver.getCurrentUrl());
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Redirection fail to login page", URL, Login_URL);

    }

    @When("User enter space in username and password and click on login button")
    public static void LoginWithSpaceInCredentials() {
        String Login_URL = "https://sample.pfinite.com/auth/login";
        Ateroid_Org.Pages.LoginPage loginPage = new Ateroid_Org.Pages.LoginPage();
        loginPage.enterLoginDetails(" ", " ");
        System.out.println(driver.getCurrentUrl());
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Redirection fail to login page", URL, Login_URL);
        sleep(1);
        String errorMsg = "These credentials do not match our records or account is not active.";
        By actualMsg = By.xpath("/html/body/div/div/form/div[1]/label");
        Assert.assertEquals("Credentials is not matched", errorMsg, get_Text(actualMsg));
    }

    @When("User enter Invalid email and try to reset password")
    public static void ForgotPasswordLinkInvalidCredentials() throws InterruptedException {
        Ateroid_Org.Pages.LoginPage loginPage = new Ateroid_Org.Pages.LoginPage();
        loginPage.ForgotPasswordwithinvalidcredentials("nishit@atliq1.com");
        String URL = driver.getCurrentUrl();
        By actualMsg = By.cssSelector(".toast-message");
        System.out.println(actualMsg);
        String errorMsg = "Please enter valid Email or Username to get OTP.";
        Assert.assertEquals("Invalid credentials", errorMsg, get_Text(actualMsg));
    }

    @Given("User enter Invalid username and password and click on login button")
    public static void LoginWithInvalidCredentials() {

        String errorMsg = "These credentials do not match our records or account is not active.";
        By actualMsg = By.className("control-label");
        Ateroid_Org.Pages.LoginPage loginPagePages = new Ateroid_Org.Pages.LoginPage();
        loginPagePages.enterLoginDetails("admin1", "Admin");
        Assert.assertEquals("Credentials is not matched", errorMsg, get_Text(actualMsg));

    }

    @Then("User should not able to login successfully and Redirected to login page")
    public static void ReturnToLogin() throws InterruptedException {
        driver.get("https://sample.pfinite.com/auth/login");
        String Login_URL = "https://sample.pfinite.com/auth/login";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Redirection fail to login page", URL, Login_URL);

    }

    @When("User Allow the Push Notification Access for all")
    public static void EnablePushNotification() throws InterruptedException {
        Ateroid_Org.Pages.LoginPage loginPagePages = new Ateroid_Org.Pages.LoginPage();
        List<WebElement> l = driver.findElements(By.xpath("//*[@id=\"onesignal-slidedown-allow-button\"]"));
        if (l.size() == 0) {
            System.out.println("Element not present");
        } else {
            loginPagePages.EnableWebPushNotification();
        }
    }

    @Then("User Should able to logout successfully and redirected to login page")
    public static void UserLogoutVerification() throws InterruptedException {
        String URL = "https://sample.ateroid.com/auth/login";
        String Logout_Url = driver.getCurrentUrl();
        Assert.assertEquals("Logout unsuccessful", URL, Logout_Url);
    }

    @Given("User is on login page")
    public void userIsOnLoginPage() {
        loginPagePages.userIsOnLoginPage();
    }

    @When("User enter valid username and password and click on login button")
    public void UserEnterValidUsernameAndPasswordAndClickOnLoginButton() throws InterruptedException {
        String userName = LoadProps.getProperty("adminUname");
        String pass = LoadProps.getProperty("adminPass");
        loginPagePages.enterLoginDetails(userName, pass);
        sleep(3);
        By actualMsg = By.cssSelector(".toast-message");
        String message = "Login successful";
        Assert.assertEquals("Credentials is not matched", message, get_Text(actualMsg));
    }

    @Then("User should able to login successfully and navigate to Dashboard page")
    public void userShouldLoginSuccessfullyAndNavigateToDashboard_page() throws InterruptedException {
        String message = "Login successful";
        String URL = "https://sample.pfinite.com/";
        By actualMsg = By.cssSelector(".toast-message");
        sleep(5);
        Assert.assertEquals("Dashboard Url not match", URL, driver.getCurrentUrl());

    }

    @When("User click on logout and redirected to login page")
    public void UserLogout() throws InterruptedException {
        //
        Ateroid_Org.Pages.LoginPage loginPage = new Ateroid_Org.Pages.LoginPage();
        List<WebElement> l = driver.findElements(By.xpath("//*[@id=\"onesignal-slidedown-allow-button\"]"));
        if (l.size() == 0) {
            System.out.println("Element not present");
        } else {
            loginPage.EnableWebPushNotification();
        }
        // **Temporary purpose**
        sleep(3);
        loginPagePages.Logout();
        sleep(1);

    }


//    @BeforeMethod
//    public void openBrowser() {
//        setUpBrowser();
//    }
//
//    @AfterMethod
//    public void closeBrowsers(Scenario args) {
//          CloseBrowser(args);
//    }
}
