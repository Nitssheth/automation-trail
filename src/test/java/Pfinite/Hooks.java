package Ateroid;


import Ateroid_Org.LoadProps;
import Ateroid_Org.Utils;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.presentation.PresentationMode;
import net.masterthought.cucumber.sorting.SortingMethod;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;

import static Ateroid_Org.BrowserSelector.loadProp;
import static Ateroid_Org.BrowserSelector.setUpBrowser;
import static org.apache.commons.io.FileUtils.copyFile;

public class Hooks extends Utils {
    public static final String browser = loadProp.getProperty("browser");
    @After
    public static void report() {
        List<String> jsonFiles = new ArrayList<>();
        jsonFiles.add("target/site/cucumber.json");
        File reportOutputDirectory = new File("target/site");
        String buildNumber = "101";
        String projectName = "Pfinite System";
        Configuration configuration = new Configuration(reportOutputDirectory, projectName);
        configuration.setBuildNumber(buildNumber);
        configuration.addClassifications("Environment", "QA");
        configuration.addClassifications("Browser", LoadProps.getProperty("browser"));
        configuration.addClassifications("Platform", System.getProperty("os.name").toUpperCase());
        configuration.setSortingMethod(SortingMethod.NATURAL);
        configuration.addPresentationModes(PresentationMode.EXPAND_ALL_STEPS);
        configuration.addPresentationModes(PresentationMode.PARALLEL_TESTING);
        configuration.setTrendsStatsFile(new File("target/test-classes/demo-trends.json"));
        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();


    }

    @Before
    public void openBrowser() throws Exception {
        long threadId = Thread.currentThread().getId();
        String processName = ManagementFactory.getRuntimeMXBean().getName();
        System.out.println("Started in thread: " + threadId + ", in JVM: " + processName);
        setUpBrowser(browser);

    }

    @After
    public void closeBrowser(Scenario args) {

        if (args.isFailed()) {
            String screenshotName = args.getName().replaceAll(".,:;?!", "") + timeStamp() + ".png";
            try {
                File sourcePath = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                File destinationPath = new File(System.getProperty("user.dir") + "/target/screenshots/" + screenshotName);
//                File storePath = new File("src/test/Screenshot/"+screenshotName);
                copyFile(sourcePath, destinationPath);
//                copyFile(sourcePath,storePath);
                System.out.print("!!!!!!......Scenario Failed....!!!!!! Please see attached screenshot for the error/issue");
                final byte[] screenshot = ((TakesScreenshot) driver)
                        .getScreenshotAs(OutputType.BYTES);
                args.attach(screenshot, "image/png", "Failed screenshot");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        driver.quit();
    }

}
