package Ateroid;

import Ateroid_Org.Pages.DashBoardPage;
import Ateroid_Org.Pages.InquiryPage;
import Ateroid_Org.Utils;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;

public class Inquiry extends Utils {

    public static String downloadPath = "/home/poojan/IdeaProjects/automation-trail/target/screenshots";
    DashBoardPage dashboardObject = new DashBoardPage();
    //    public By _RedirectionToList = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/div/div/div[1]/a");
    InquiryPage InquiryObj = new InquiryPage();
    String stat = null;

    @Then("User should redirected to inquiry module")
    public void InquiryRedirection() throws InterruptedException {
        dashboardObject.CardCheck("Inquiry");
        String Login_URL = "https://sample.pfinite.com/inquiries";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Redirection fail to Inquiry", URL, Login_URL);
    }

    @Then("User verify the inquiry filters and list export")
    public void ExportCsv() throws InterruptedException {
        DashboardInquiry();
        WebElement elementCSV = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[4]/div/div[1]/div[1]/div[1]/a"));
        elementCSV.click();
        sleep(5);
        File file = getLatestFileFromDir(downloadPath);
        String csvFileName = file.getName();
        System.out.println("CSV File Downloaded is :- " + csvFileName);
    }

    public File getLatestFileFromDir(String dirPath) {
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }

    @Then("User verify the dashboard page and redirect to inquiry")
    public void DashboardInquiry() throws InterruptedException {
        dashboardObject.DashboardCard();
        InquiryObj.InquiryFilters();
        InquiryObj.InquirySearch("Inq1");
    }

    @Then("User Should able to create new Inquiry")
    public void VerifyInquiry() throws InterruptedException {
        String Login_URL = "https://sample.pfinite.com/inquiries";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Inquiry fail to create", URL, Login_URL);
    }

    @Then("User try to create new Inquiry")
    public void CreateInquiry() throws InterruptedException {
        InquiryObj.createInquiry();
    }

    @When("User try to Edit the inquiry for {string} or {string} in inquiry")
    public void EditInquiries(String st1, String st2) throws InterruptedException {
        InquiryObj.InquiryEdit(st1, st2);
    }

    @When("User check status is {string} or {string} in edit inquiry")
    public void UserCheckStatus(String st1, String st2) {
        if (st1.equals(InquiryObj.getStatus())) {
            stat = st1;

        } else {
            stat = st2;
        }
        System.out.println("Project is " + stat);
    }


    @Then("Status DropDown field should have {string} or {string}")
    public void StatusVerification(String st1, String st2) {
        if (InquiryObj.currentStatus.contains(stat)) {
            System.out.println("Status options available");
        } else {
            System.out.println("Status options is not available");
        }
    }

    @Then("User Verify the updated inquiry and redirected to list screen")
    public void VerifyUpdateAndRedirectedToList() {
        String Login_URL = "https://sample.pfinite.com/inquiries";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Inquiry fail to update", URL, Login_URL);
    }

    @Then("User try to delete the inquiry")
    public void DeleteInquiry() {
        InquiryObj.DeleteInquiry();
    }

    @Then("User Should able to delete the inquiry")
    public void VerifyUserAbleToDeleteInquirySuccessfully() {
        By actualMsg = By.cssSelector(".toast-message");
        String message = "Successfully deleted";
        Assert.assertEquals("Credentials is not matched", message, get_Text(actualMsg));
    }


}
