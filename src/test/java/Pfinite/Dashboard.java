package Ateroid;

import Ateroid_Org.Pages.DashBoardPage;
import Ateroid_Org.Utils;
import io.cucumber.java.en.Then;

public class Dashboard extends Utils {

    DashBoardPage dashboardObject = new DashBoardPage();

    @Then("User verify the dashboard page by clicking on cards")
    public void Dashboard_Card() throws InterruptedException {
        dashboardObject.DashboardCard();
    }

    @Then("User should redirected to module by clicking card {string}")
    public void CardRedirection(String CardModule) {
        dashboardObject.CardCheck(CardModule);

    }

    @Then("User should redirected to particular module through {string} and redirected to the particular page")
    public void SideMenu(String Module) {
        dashboardObject.SideMenu(Module);
    }


}
