package Ateroid;

import Ateroid_Org.Pages.ServiceCallPage;
import Ateroid_Org.Utils;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.util.Iterator;
import java.util.Set;

public class ServiceCall extends Utils {
    public static String downloadPath = "/home/poojan/IdeaProjects/automation-trail/target/screenshots";

    ServiceCallPage servicePage = new ServiceCallPage();

    @Then("User verify redirection to ServiceCall")
    public void redirectionToServiceCallList() {
        servicePage.serviceCallList();
    }

    @When("User should redirected to ServiceCall")
    public void verifyServiceCallList() {
        String Login_URL = "https://sample.pfinite.com/service-calls";
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("Redirection failed to ServiceCall", URL, Login_URL);
    }

    @When("User verify the filters in ServiceCall list")
    public void servicePageListFilters(){
        servicePage.serviceCallFilters();
    }

    @When("User verify the search feature in ServiceCall list")
    public void servicePageListSearch(){
        servicePage.serviceCallSearch("Test");
    }

    @Then("User should able to search in ServiceCall list")
    public void verifyServicePageSearch(){
        WebElement label = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[4]/div/div[1]/div[2]/form/div/input"));
        String Value = label.getAttribute("value");
        System.out.println(Value);
        String searchLabel = "Test";
        Assert.assertEquals("Search failed",searchLabel,Value);
    }

    @When("User try to create new ServiceCall")
    public void createServiceCall(){
        servicePage.createServiceCall();
    }

    @Then("User should able to create new ServiceCall")
    public void verifyServiceCall(){
        String Login_URL = "https://sample.pfinite.com/service-calls";
        String URL = driver.getCurrentUrl();
        if (URL.contains("https://sample.pfinite.com/service-calls/create")) {
            System.out.println("Service call already created for this User");
        } else {
            Assert.assertEquals("ServiceCall fail to create", Login_URL, URL);
        }
    }


    @When("User try to edit the ServiceCall")
    public void editInward(){
        servicePage.serviceCallOptions("Edit");
    }

    @Then("User should able to edit the ServiceCall")
    public void verifyEditInward() throws InterruptedException {
        String Login_URL = "https://sample.pfinite.com/service-calls";
        sleep(2);
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("ServiceCall fail to update", URL, Login_URL);
    }

    @When("User try to close the ServiceCall")
    public void closeInward(){
        servicePage.serviceCallOptions("Close");
    }

    @Then("User should able to close the ServiceCall")
    public void verifyCloseInward() throws InterruptedException {
        String Login_URL = "https://sample.pfinite.com/service-calls";
        sleep(2);
        String URL = driver.getCurrentUrl();
        Assert.assertEquals("ServiceCall fail to update", URL, Login_URL);
    }
    @When("User try to delete the ServiceCall")
    public void deleteServiceCall(){
        servicePage.serviceCallOptions("Delete");
    }

    @Then("User should able to delete the ServiceCall")
    public void verifyDeleteServiceCall(){
        By actualMessage = By.cssSelector("div > div.toast-message");
        String message = "Successfully deleted";
        Assert.assertEquals("Inward deleted successfully.",message,get_Text(actualMessage));
    }


    @When("User try to print the ServiceCall")
    public void printServiceCall(){
        servicePage.serviceCallOptions("Print");
    }
    @Then("User should able to print the ServiceCall")
    public void verifyPrintServiceCall(){
    String mainWindowHandle = driver.getWindowHandle();
    Set<String> allWindowHandles = driver.getWindowHandles();
    Iterator<String> iterator = allWindowHandles.iterator();
        while (iterator.hasNext()) {
        String ChildWindow = iterator.next();
        driver.switchTo().window(ChildWindow);
        String URls = driver.getCurrentUrl();
        if (!mainWindowHandle.equalsIgnoreCase(ChildWindow) && URls.contains("https://sample.ateroid.com/print-service-call")) {
            System.out.println("Print success");
            break;
         }
        }
    }
    @When("User try to view the ServiceCall")
    public void viewServiceCall() {
        servicePage.serviceCallOptions(" ");
    }

    @Then("User should able to view the ServiceCall")
    public void verifyViewServiceCall() {
        boolean view = false;
        String URL = driver.getCurrentUrl();
        if (URL.contains("https://sample.pfinite.com/service-calls/")) {
            view = true;
            System.out.println("View success");
        } else {
            System.out.println("View failed");
        }
    }

    @When("User verify the serviceCall list export")
    public void ExportCsv() throws InterruptedException {
        clickOnElement(By.xpath("/html/body/div/aside/div/section/ul/li[6]/a/span"));
        WebElement elementCSV = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[1]/div[1]/div[1]/a/i"));
        elementCSV.click();
        sleep(5);
    }

    @Then("User should able to export the serviceCall")
    public void VerifyCsv() throws InterruptedException {
        File file = getLatestFileFromDir(downloadPath);
        String csvFileName = file.getName();
        System.out.println("CSV File Downloaded is :- " + csvFileName);
    }

    public File getLatestFileFromDir(String dirPath) {
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return null;
        }

        File lastModifiedFile = files[0];
        for (int i = 1; i < files.length; i++) {
            if (lastModifiedFile.lastModified() < files[i].lastModified()) {
                lastModifiedFile = files[i];
            }
        }
        return lastModifiedFile;
    }


}

