package Runner;
package Ateroid;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/",
        monochrome = true,
        plugin = {"json:target/cucumber.json", "pretty",
                "de.monochromata.cucumber.report.PrettyReports:target/cucumber"},
        glue = "Ateroid",
        publish = true)

public class RunTest {


}


