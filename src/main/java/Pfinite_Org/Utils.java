package Ateroid_Org;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Utils extends BaseClass {

    public static void enterText(By by, String text) {
        driver.findElement(by).clear();
        driver.findElement(by).sendKeys(text);
    }

    public static boolean isElementpresent(By by) {
        return driver.findElement(by).isDisplayed();
    }

    public static String get_Text(By by) {

        return driver.findElement(by).getText();
    }

    public static void clickOnElement(By by) {

        driver.findElement(by).click();
    }

    public static void webDriverWaitImplicitly(int time) {
        driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
    }

    public static void webDriverWaitExplicitly(By by, int time) {
        WebDriverWait explicitlyWait = new WebDriverWait(driver, time);
        explicitlyWait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static String timeStamp() {
        DateFormat dateFormat = new SimpleDateFormat("ddMMYY-HHmmss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static void sleep(int second) {
        try {
            Thread.sleep(second * 1000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void UploadFile(By by, By args) {
        WebElement addFile = driver.findElement(by);
        File file =new File("src/test/resources/TestData/SampleImage.jpg");
        addFile.sendKeys(file.getAbsolutePath());
        if (driver.findElement(args).isDisplayed()) {
            Assert.assertTrue("Image Uploaded", true);
        } else {
            Assert.fail("Image not Uploaded");
        }

    }

    public static void selectElementByValue(By by, String text) {
        Select select = new Select(driver.findElement(by));
        select.selectByValue(text);
    }

    public static void selectElementByVisibletext(By by, String text) {
        Select select = new Select(driver.findElement(by));
        select.selectByVisibleText(text);
    }

    public static void selectElementByIndex(By by, int integer) {
        Select select = new Select(driver.findElement(by));
        select.selectByIndex(integer);
    }

//    JFileChooser
}
