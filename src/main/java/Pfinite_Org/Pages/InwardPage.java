package Ateroid_Org.Pages;

import Ateroid_Org.EmailUtils;
import Ateroid_Org.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class InwardPage extends Utils {
    private static EmailUtils emailUtils;
    public By _outwardAttachment = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[9]/div/div/div[1]/div/div[1]/div[1]");
    public By _toRepairerRemark = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[2]/div/textarea");
    public By _list = By.xpath("/html/body/div/aside/div/section/ul/li[5]/a");
    public By _allFilter = By.xpath("/html/body/div/div/div/section[2]/div/div/div/ul/li[1]/a");
    public By _inwardFilter = By.xpath("/html/body/div/div/div/section[2]/div/div/div/ul/li[2]/a");
    public By _toRepairerFilter = By.xpath("/html/body/div/div/div/section[2]/div/div/div/ul/li[3]/a");
    public By _fromRepairerFilter = By.xpath("/html/body/div/div/div/section[2]/div/div/div/ul/li[4]/a");
    public By _outwardFilter = By.xpath("/html/body/div/div/div/section[2]/div/div/div/ul/li[5]/a");
    public By _inwardSearch = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[5]/div/div[1]/div[2]/form/div/input");
    public By _backToList = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div/div[1]/a");
    public By _back = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div/div[2]/a");
    public By _clickSearch = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[5]/div/div[1]/div[2]/form/div/div/button");
    public By _inwardCreate = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[1]/div[1]/div[2]/a");
    public By _inwardRemark = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[2]/div/textarea");
    public By _inwardTaken = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/form/div[1]/div/div[1]/div[3]/div/span/span[1]");
    public By _inwardTakenSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _inwardTakenEngineer = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _inwardEngineer = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/form/div[1]/div/div[1]/div[4]/div/span/span[1]");
    public By _inwardSearchEngineer = By.xpath("/html/body/span/span/span[1]/input");
    public By _inwardSelectEngineer = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _inwardFileVerify = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/div/div/div[4]/div[1]/input");
    public By _inwardAmount = By.xpath("//*[@id=\"inward_amount\"]");
    public By _inwardAccessories = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[1]/div/textarea");
    public By _inwardCustomer = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[3]/div/span/span[1]");
    public By _inwardSearchCustomer = By.xpath("/html/body/span/span/span[1]/input");
    public By _inwardSelectCustomer = By.xpath("/html/body/span/span/span[2]/ul/li[2]");
//    public By _inwardProductCategory = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]");
    public By _inwardProductSearch = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public By _inwardProductSelect = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li");
    public By _brand = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/label");
    public By _inwardBrand = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/div/span/span[1]/span/span[1]/span");
    public By _inwardBrandSelect = By.xpath("/html/body/span/span/span[2]/ul/li");
    public By _inwardContract = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[7]/div/span/span[1]");
    public By _inwardContractSelect = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _inwardProblemType = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[8]/div/span/span[1]");
    public By _inwardProblemTypeSelect = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[1]");
    public By _create = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[2]/div[2]/div/button");
    public By _editButton = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[1]/td[13]/a[1]");
    public By _editAccessories = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[1]/div[2]/div/textarea");
    public By _editSelectAssigneeBox = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[1]/div[3]/div/span/span[1]/span/span[1]");
    public By _editSearchAssigneeBox = By.xpath("/html/body/span/span/span[1]/input");
    public By _editSelectAssignee = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _editSearchProductCategory = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[1]/div[4]/div/span/span[1]/span/ul");
    public By _editSelectProductCategory = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[2]");
    public By _editRemoveProductCategory = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[1]/div[4]/div/span/span[1]/span/ul/li[1]/span");
    public By _editSelectProblemTypeBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[1]/div[5]/div/span/span[1]/span/ul");
    public By _editSelectProblemType = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[2]");
    public By _editRemoveProblemType = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[1]/div[5]/div/span/span[1]/span/ul/li[1]/span");
    public By _editInwardCustomer = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[2]/div[3]/div/span/span[1]/span/span[1]");
    public By _editInwardSearchCustomer = By.xpath("/html/body/span/span/span[1]/input");
    public By _editInwardSelectCustomer = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _editBrand = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/span[1]");
    public By _editInwardBrandSelect = By.xpath("/html/body/span/span/span[2]/ul/li");
    public By _editInwardContract = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[2]/div[6]/div/span/span[1]/span");
    public By _editInwardContractSelect = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _updateInward = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[2]/div/div/button");
    public By _confirmDeleteInward = By.xpath("/html/body/div[2]/div/div[3]/button[1]");
    public By _outwardRemark = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[2]/div/textarea");
    public By _outwardTakenBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[3]/div/span/span[1]/span/span[1]");
    public By _outwardTakenSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _outwardTakenSelect = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _outwardEngineerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[4]/div/span/span[1]/span/span[1]");
    public By _outwardEngineerSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _outwardEngineerSelect = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _outwardAmount = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[5]/div/div/input");
    public By _outwardDate = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/div/div/input");
    public By _outwardPaymentModeBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/div/span/span[1]/span/span[1]");
    public By _outwardPaymentModeSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _outwardPaymentModeSelect = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _outwardReceiptNumber = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[8]/div/div/input");
    public By _toRepairerAttendedEngineerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[3]/div/span/span[1]/span/span[1]");
    public By _outwardSendOTP = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[11]/div/a[1]");
    public By _outwardAccessories = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[1]/div/textarea");
    public By _outwardCustomer = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[3]/div/span/span[1]/span/span[1]");
    public By _outwardSearchCustomer = By.xpath("/html/body/span/span/span[1]/input");
    public By _outwardSelectCustomer = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _outwardProductSearch = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public By _outwardProductSelect = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[2]");
    public By _outwardBrand = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/div/span/span[1]/span/span[1]");
    public By _outwardBrandSelect = By.xpath("/html/body/span/span/span[2]/ul/li");
    public By _outwardContract = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[7]/div/span/span[1]/span/span[1]");
    public By _outwardContractSelect = By.xpath("/html/body/span/span/span[2]/ul/li[3]");
    public By _outwardSolution = By.cssSelector("#has-many-problem_type_many > div > div > div:nth-child(2) > div > textarea");
    public By _otpTxt = By.id("otp");
    public By _invalidOTP = By.xpath("/html/body/div[1]/div/div/section[2]/div[1]/h4");
    public By _submitOutward = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[2]/div[2]/div/button");
    public By _toRepairerSearchAttendedEngineer = By.xpath("/html/body/span/span/span[1]/input");
    public By _toRepairerSelectAttendedEngineer = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _toRepairerAssignedEngineerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[4]/div/span/span[1]/span/span[1]");
    public By _toRepairerSearchAssignedEngineer = By.xpath("/html/body/span/span/span[1]/input");
    public By _toRepairerSelectAssignedEngineer = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _toRepairerStartDate = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[5]/div/div/input");
    public By _toRepairerEstimatedDateLabel = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/label");
    public By _toRepairerEstimatedDate = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/div/div/input");
    public By _toRepairerVendorLabel = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/label");
    public By _toRepairerVendorBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/div/span/span[1]/span/span[1]");
    public By _toRepairerVendorSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _toRepairerVendorSelect = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _toRepairerCourierBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[8]/div/span/span[1]/span/span[1]");
    public By _toRepairerCourierSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _toRepairerCourierSelect = By.xpath("/html/body/span/span/span[2]/ul/li");
    public By _toRepairerCourierNumber = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[9]/div/div/input");
    public By _toRepairerAttachment = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[10]/div/div/div[4]/div[1]/input");
    public By _toRepairerAccessories = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[1]/div/textarea");
    public By _toRepairerCustomerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[3]/div/span/span[1]/span/span[1]");
    public By _toRepairerCustomerSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _toRepairerCustomerSelect = By.xpath("/html/body/span/span/span[2]/ul/li[2]");
    public By _toRepairerProductSearch = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public By _toRepairerProductSelect = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[2]");
    public By _toBrand = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/label");
    public By _toRepairerBrand = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/div/span/span[1]/span/span[1]/span");
    public By _toRepairerBrandSelect = By.xpath("/html/body/span/span/span[2]/ul/li");
    public By _toRepairerContract = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[7]/div/span/span[1]");
    public By _toRepairerContractSelect = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _toRepairerProblemType = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[8]/div/span/span[1]");
    public By _toRepairerProblemTypeSelect = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[1]");
    public By _toRepairerUpdate = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[2]/div[2]/div/button");
    public By _fromRepairerRemark = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[2]/div/textarea");
    public By _fromRepairerAttendedEngineerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[3]/div/span/span[1]/span/span[1]");
    public By _fromRepairerSearchAttendedEngineer = By.xpath("/html/body/span/span/span[1]/input");
    public By _fromRepairerSelectAttendedEngineer = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _fromRepairerAssignedEngineerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[4]/div/span/span[1]/span/span[1]");
    public By _fromRepairerSearchAssignedEngineer = By.xpath("/html/body/span/span/span[1]/input");
    public By _fromRepairerSelectAssignedEngineer = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public By _fromRepairerReturnDate = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[5]/div/div/input");
    public By _fromRepairerVendorLabel = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/label");
    public By _fromRepairerCourierBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/div/span/span[1]/span/span[1]");
    public By _fromRepairerCourierSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _fromRepairerCourierSelect = By.xpath("/html/body/span/span/span[2]/ul/li");
    public By _fromRepairerCourierNumber = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/div/div/input");
    public By _fromRepairerAttachment = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[8]/div/div/div[4]/div[1]/input");
    public By _fromRepairerAccessories = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[1]/div/textarea");
    public By _fromRepairerCustomerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[3]/div/span/span[1]/span/span[1]");
    public By _fromRepairerCustomerSearch = By.xpath("/html/body/span/span/span[1]/input");
    public By _fromRepairerCustomerSelect = By.xpath("/html/body/span/span/span[2]/ul/li[2]");
    public By _fromRepairerProductSearch = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public By _fromRepairerProductSelect = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[2]");
    public By _fromBrand = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/label");
    public By _fromRepairerBrand = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/div/span/span[1]/span/span[1]/span");
    public By _fromRepairerBrandSelect = By.xpath("/html/body/span/span/span[2]/ul/li");
    public By _fromRepairerContract = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[7]/div/span/span[1]");
    public By _fromRepairerContractSelect = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _fromRepairerProblemType = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[8]/div/span/span[1]");
    public By _fromRepairerProblemTypeSelect = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[1]");
    public By _fromRepairerUpdate = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[2]/div[2]/div/button");
    int i;
    int j;


    public static void connectToEmail() {
        try {
            emailUtils = new EmailUtils("nishitshethinfo@gmail.com", "Nits@131805", "smtp.gmail.com", EmailUtils.EmailFolder.INBOX);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    public void inwardList(){
        clickOnElement(_list);
    }

    public void inwardFilters(){
        isElementpresent(_allFilter);
        clickOnElement(_allFilter);
        isElementpresent(_inwardFilter);
        clickOnElement(_inwardFilter);
        isElementpresent(_toRepairerFilter);
        clickOnElement(_toRepairerFilter);
        isElementpresent(_fromRepairerFilter);
        clickOnElement(_fromRepairerFilter);
        isElementpresent(_outwardFilter);
        clickOnElement(_outwardFilter);
    }

    public void inwardSearch(String Search){
        isElementpresent(_inwardSearch);
        clickOnElement(_inwardSearch);
        enterText(_inwardSearch, Search);
        clickOnElement(_clickSearch);
    }

    public void createInward() throws ArrayIndexOutOfBoundsException{
        isElementpresent(_inwardCreate);
        clickOnElement(_inwardCreate);
        clickOnElement(_backToList);
        isElementpresent(_inwardCreate);
        clickOnElement(_inwardCreate);
        clickOnElement(_back);
        clickOnElement(_inwardCreate);
        isElementpresent(_inwardRemark);
        enterText(_inwardRemark,"Inward Remark");
        clickOnElement(_inwardTaken);
        enterText(_inwardTakenSearch," ");
        clickOnElement(_inwardTakenEngineer);
        clickOnElement(_inwardEngineer);
        enterText(_inwardSearchEngineer," ");
        clickOnElement(_inwardSelectEngineer);
        enterText(_inwardAmount,"250");
        UploadFile(By.xpath(".//input[@type='file']"),_inwardFileVerify );
        enterText(_inwardAccessories,"xxxx xxxx xxxx xxxx");
        clickOnElement(_inwardCustomer);
        enterText(_inwardSearchCustomer," ");
        clickOnElement(_inwardSelectCustomer);
        enterText(_inwardProductSearch," ");
        clickOnElement(_inwardProductSelect);
        clickOnElement(_brand);
        clickOnElement(_inwardBrand);
        clickOnElement(_inwardBrandSelect);
        clickOnElement(_inwardContract);
        clickOnElement(_inwardContractSelect);
        clickOnElement(_inwardProblemType);
        clickOnElement(_inwardProblemTypeSelect);
        clickOnElement(_create);
        sleep(2);

    }

    public void editInward() throws ArrayIndexOutOfBoundsException{

        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("Edit")) {
                    By _editInward = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]");
                    isElementpresent(_editInward);
                    clickOnElement(_editInward);
                    enterText(_editAccessories, " -Edit");
                    clickOnElement(_editSelectAssigneeBox);
                    enterText(_editSearchAssigneeBox, " ");
                    clickOnElement(_editSelectAssignee);
                    clickOnElement(_editSearchProductCategory);
                    clickOnElement(_editSelectProductCategory);
                    clickOnElement(_editRemoveProductCategory);
                    sleep(1);
                    clickOnElement(_editSelectProblemTypeBox);
                    clickOnElement(_editSelectProblemType);
                    clickOnElement(_editRemoveProblemType);
                    sleep(1);
                    clickOnElement(_editInwardCustomer);
                    enterText(_editInwardSearchCustomer, " ");
                    clickOnElement(_editInwardSelectCustomer);
                    clickOnElement(_editBrand);
                    clickOnElement(_editInwardBrandSelect);
                    clickOnElement(_editInwardContract);
                    clickOnElement(_editInwardContractSelect);
                    clickOnElement(_updateInward);
                    break outerLoop;
                }
            }

        }


    }

    public void deleteInward() throws ArrayIndexOutOfBoundsException{
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("Delete")) {
                    By _deleteInward = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]");
                    isElementpresent(_deleteInward);
                    clickOnElement(_deleteInward);
                    sleep(2);
                    clickOnElement(_confirmDeleteInward);
                    break outerLoop;
                }
            }

        }

    }

    public void outward() throws ArrayIndexOutOfBoundsException{
        boolean invalid = true;
        int i;
        int j;
        boolean outward = false;

        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 0; i <= rows.size(); i++) {
            if (outward != true) {
                List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a"));
                for (j = 1; j <= options.size(); j++) {
                    String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]")).getAttribute("title");
                    if (titleAttribute.equals("Outward")) {
                        clickOnElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]"));
                        isElementpresent(_outwardRemark);
                        enterText(_outwardRemark, "Outward");
                        clickOnElement(_outwardTakenBox);
                        enterText(_outwardTakenSearch, " ");
                        clickOnElement(_outwardTakenSelect);
                        clickOnElement(_outwardEngineerBox);
                        enterText(_outwardEngineerSearch, " ");
                        clickOnElement(_outwardEngineerSelect);
                        enterText(_outwardAmount, "2500");
                        clickOnElement(_outwardDate);
                        clickOnElement(By.cssSelector("#outward_outward_date"));
                        clickOnElement(By.cssSelector(".today"));
                        clickOnElement(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/label"));
                        clickOnElement(_outwardPaymentModeBox);
                        enterText(_outwardPaymentModeSearch, " ");
                        clickOnElement(_outwardPaymentModeSelect);
                        enterText(_outwardReceiptNumber, "#0023");
                        UploadFile(By.xpath(".//input[@type='file']"), _outwardAttachment);
                        clickOnElement(_outwardSendOTP);
                        sleep(3);
                        enterOTPForOutward();
                        enterText(_outwardAccessories, "Accessories");
                        clickOnElement(_outwardCustomer);
                        enterText(_outwardSearchCustomer, " ");
                        clickOnElement(_outwardSelectCustomer);
                        enterText(_outwardProductSearch, " ");
                        clickOnElement(_outwardProductSelect);
                        clickOnElement(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/label"));
                        clickOnElement(_outwardBrand);
                        clickOnElement(_outwardBrandSelect);
                        clickOnElement(_outwardContract);
                        clickOnElement(_outwardContractSelect);
                        if (_otpTxt != null) {
                            if (isElementpresent(_outwardSolution)) {
                                enterText(_outwardSolution, "Solution 1");
                            }
                            clickOnElement(_submitOutward);
                            outward = true;
                        } else {
                            enterOTPForOutward();
                        }
                        while (invalid) {
                            try {
                                invalid = isElementpresent(_invalidOTP);
                                if (invalid) {
                                    clickOnElement(_outwardTakenBox);
                                    enterText(_outwardTakenSearch, " ");
                                    clickOnElement(_outwardTakenSelect);
                                    enterOTPForOutward();
                                    clickOnElement(_submitOutward);
                                    invalid = false;
                                    outward = true;
                                }
                            } catch (Exception NoSuchElementException) {
                                System.out.println("Outward Successfully.");
                                break;
                            }
                            //            WebElement test = driver.findElement(By.xpath("/html/body/div[1]/div/div/section[2]/div[1]/h4"));
                            //            System.out.println(test);
                            //            if(test.equals(null)) {
                            //                invalid = true;
                            //                break;
                            //
                        }
                        break outerLoop;
                    } else {
                        System.out.println("No outward option is available");
                    }
                }

            }
        }
    }

    public void printInward() throws ArrayIndexOutOfBoundsException{
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("Print")) {
                    By _printInward = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]");
                    isElementpresent(_printInward);
                    clickOnElement(_printInward);
                    String link = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]")).getAttribute("href");
                    System.out.println(link);
                    break outerLoop;
                }
            }

        }

    }

    public void enterOTPForOutward() {
        connectToEmail();
        try {
            String verificationCode = emailUtils.getAuthorizationOTP();
            enterText(_otpTxt, verificationCode);
            sleep(3);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

    }

    public void viewInward() throws ArrayIndexOutOfBoundsException{
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("View")) {
                    By _viewInward = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]");
                    isElementpresent(_viewInward);
                    clickOnElement(_viewInward);
                    break;
                }
            }

        }

    }

    public void toRepairer() throws ArrayIndexOutOfBoundsException{
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("To Repairer")) {
                    By _toRepairer = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]");
                    isElementpresent(_toRepairer);
                    clickOnElement(_toRepairer);
                    enterText(_toRepairerRemark, "To repairer remark");
                    clickOnElement(_toRepairerAttendedEngineerBox);
                    enterText(_toRepairerSearchAttendedEngineer, " ");
                    clickOnElement(_toRepairerSelectAttendedEngineer);
                    clickOnElement(_toRepairerAssignedEngineerBox);
                    enterText(_toRepairerSearchAssignedEngineer, " ");
                    clickOnElement(_toRepairerSelectAssignedEngineer);
                    clickOnElement(_toRepairerStartDate);
                    clickOnElement(By.cssSelector("#to_repairer_start_date"));
                    clickOnElement(By.cssSelector(".today"));
                    clickOnElement(_toRepairerEstimatedDateLabel);
                    clickOnElement(_toRepairerEstimatedDate);
                    clickOnElement(By.cssSelector("#to_repairer_estimated_return_date"));
                    clickOnElement(By.cssSelector(".today"));
                    clickOnElement(_toRepairerVendorLabel);
                    clickOnElement(_toRepairerVendorBox);
                    enterText(_toRepairerVendorSearch, " ");
                    clickOnElement(_toRepairerVendorSelect);
                    clickOnElement(_toRepairerCourierBox);
                    enterText(_toRepairerCourierSearch, " ");
                    clickOnElement(_toRepairerCourierSelect);
                    enterText(_toRepairerCourierNumber, "123456");
                    UploadFile(By.xpath(".//input[@type='file']"), _toRepairerAttachment);
                    enterText(_toRepairerAccessories, "To RepairerAccessories");
                    clickOnElement(_toRepairerCustomerBox);
                    enterText(_toRepairerCustomerSearch, " ");
                    clickOnElement(_toRepairerCustomerSelect);
                    clickOnElement(_toRepairerProductSearch);
                    clickOnElement(_toRepairerProductSelect);
                    sleep(1);
                    clickOnElement(_toBrand);
                    clickOnElement(_toRepairerBrand);
                    clickOnElement(_toRepairerBrandSelect);
                    clickOnElement(_toRepairerContract);
                    clickOnElement(_toRepairerContractSelect);
                    clickOnElement(_toRepairerProblemType);
                    clickOnElement(_toRepairerProblemTypeSelect);
                    clickOnElement(_toRepairerUpdate);
                    System.out.println("ToRepairer Updated Successfully.");
                    break outerLoop;
                } else {
                    System.out.println("To repairer not available.");
                }
            }
        }
    }

    public void fromRepairer() throws ArrayIndexOutOfBoundsException{
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("From Repairer")) {
                    By _fromRepairer = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[13]/a[" + j + "]");
                    isElementpresent(_fromRepairer);
                    clickOnElement(_fromRepairer);
                    enterText(_fromRepairerRemark, "From repairer remark");
                    clickOnElement(_fromRepairerAttendedEngineerBox);
                    enterText(_fromRepairerSearchAttendedEngineer, " ");
                    clickOnElement(_fromRepairerSelectAttendedEngineer);
                    clickOnElement(_fromRepairerAssignedEngineerBox);
                    enterText(_fromRepairerSearchAssignedEngineer, " ");
                    clickOnElement(_fromRepairerSelectAssignedEngineer);
                    clickOnElement(_fromRepairerReturnDate);
                    clickOnElement(By.cssSelector("#from_repairer_return_date"));
                    clickOnElement(By.cssSelector(".today"));
                    clickOnElement(_fromRepairerVendorLabel);
                    clickOnElement(_fromRepairerCourierBox);
                    enterText(_fromRepairerCourierSearch, " ");
                    clickOnElement(_fromRepairerCourierSelect);
                    enterText(_fromRepairerCourierNumber, "123456");
                    isElementpresent(_fromRepairerAttachment);
                    UploadFile(By.xpath(".//input[@type='file']"), _fromRepairerAttachment);
                    enterText(_fromRepairerAccessories, "To RepairerAccessories");
                    clickOnElement(_fromRepairerCustomerBox);
                    enterText(_fromRepairerCustomerSearch, " ");
                    clickOnElement(_fromRepairerCustomerSelect);
                    clickOnElement(_fromRepairerProductSearch);
                    clickOnElement(_fromRepairerProductSelect);
                    sleep(1);
                    clickOnElement(_fromBrand);
                    clickOnElement(_fromRepairerBrand);
                    clickOnElement(_fromRepairerBrandSelect);
                    clickOnElement(_fromRepairerContract);
                    clickOnElement(_fromRepairerContractSelect);
                    clickOnElement(_fromRepairerProblemType);
                    clickOnElement(_fromRepairerProblemTypeSelect);
                    clickOnElement(_fromRepairerUpdate);
                    System.out.println("From Repairer Updated Successfully.");
                    break outerLoop;
                } else {
                    System.out.println("From repairer not available.");
                }

            }
        }
    }


    public void inwardOptions(String title) {
//        int i = 0;
//        int j = 0;
//        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
//        List<WebElement> options = driver.findElements(By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td[13]/a"));
//
//        for (i = 0; i <= rows.size(); i++) {
//            for (j = 1; j <= options.size() + 1; j++) {
//
//                System.out.println(options);
//                WebElement titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr/td[13]/a[" + j + "]"));
//                System.out.println(titleAttribute);
        switch (title) {

            case "Outward":
                outward();
                System.out.println("Outward successfully.");
                break;

            case "To Repairer":
                toRepairer();
                System.out.println("inward send to ToRepairer successfully.");
                break;

            case "From Repairer":
                fromRepairer();
                System.out.println("inward send to From Repairer successfully.");
                break;

            case "Delete":
                deleteInward();
                System.out.println("Delete successfully.");
                break;

            case "Edit":

                editInward();
                System.out.println("Edit successfully.");
                break;

            case "Print":

                printInward();
                System.out.println("Print successfully.");
                break;


            default:
                viewInward();
                System.out.println("View successfully.");
        }


    }

}
