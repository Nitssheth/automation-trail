package Ateroid_Org.Pages;

import Ateroid_Org.EmailUtils;
import Ateroid_Org.LoadProps;
import Ateroid_Org.Utils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class LoginPage extends Utils {

    private static EmailUtils emailUtils;
    public String successPassChangeUrl = "https://sample.pfinite.com/auth/login";
    public By _tostMsg = By.cssSelector(".toast-message");
    public By _ReturnToLogin = By.linkText("Login");
    public By _SubmitOTP = By.xpath("//*[@id=\"forgot_password\"]/div[3]/div/button[2]");
    public By _ResendOTP = By.xpath("//*[@id=\"forgot_password\"]/div[4]/div[1]/div/a");
    public By _OnesignalNotification = By.xpath("//*[@id=\"onesignal-slidedown-allow-button\"]");
    public String pageUrl = LoadProps.getProperty("baseUrl");
    public By _otpTxt = By.id("otp");
    public By _submitOTPBtn = By.xpath("//button[text()='Submit OTP']");
    public By _newPassTxt = By.id("password");
    public By _confPassTxt = By.id("confirm_password");
    public By _restPassBtn = By.xpath("//button[text()='Reset Password']");
    public By _EmailTxt = By.name("email");
    public By _unameTxt = By.name("username");
    public By _passTxt = By.name("password");
    public By _loginBtn = By.xpath("/html/body/div/div/form/div[4]/div/button");
    public By _remember = By.xpath("/html/body/div/div/form/div[3]/div[1]/div/label/div/ins");
    public By _ForgotPassLink = By.xpath("/html/body/div/div/form/div[3]/div[2]/div/a");
    public By _GetOTP = By.xpath("//*[@id=\"forgot_password\"]/div[3]/div/button[1]");
    public By _Profile = By.xpath("/html/body/div/header/nav/div/ul/li[2]/a/span");
    public By _Logout = By.xpath("/html/body/div/header/nav/div/ul/li[2]/ul/li[3]/a");

    public static void connectToEmail() {
        try {
            emailUtils = new EmailUtils("nishitshethinfo@gmail.com", "Nits@981305", "smtp.gmail.com", EmailUtils.EmailFolder.INBOX);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
    }

    public void userIsOnLoginPage() {
        Assert.assertTrue(isElementpresent(_loginBtn));
    }

    public void enterLoginDetails(String username, String password) {
        System.out.println("Username: " + username + " " + "Password: " + password);
        enterText(_unameTxt, username);
        enterText(_passTxt, password);
        clickOnElement(_remember);
        clickOnElement(_loginBtn);

    }

    public void ForgotPassword(String Email) {
        clickOnElement(_ForgotPassLink);
        enterText(_EmailTxt, Email);
        clickOnElement(_GetOTP);
        clickOnElement(_SubmitOTP);
//           clickOnElement(_ResendOTP);

    }


    public void ForgotPasswordwithinvalidcredentials(String Email) {
        clickOnElement(_ForgotPassLink);
        enterText(_EmailTxt, Email);
        clickOnElement(_GetOTP);
    }

    public void EnableWebPushNotification() {
        clickOnElement(_OnesignalNotification);

    }


    public void Logout() {
        clickOnElement(_Profile);
        clickOnElement(_Logout);

    }

    public void EnterOTPAndChangePassword() {
        connectToEmail();
        boolean reset = true;
        while (reset) {
            try {
                String verificationCode = emailUtils.getAuthorizationCode();
                enterText(_otpTxt, verificationCode);
                sleep(3);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
            clickOnElement(_submitOTPBtn);
            List<WebElement> _InvalidOtp = driver.findElements(By.cssSelector("#toast-container > div > div.toast-title"));
            if (_InvalidOtp.size() == 1) {
                reset = true;
            } else {
                enterText(_newPassTxt, "Admin@123");
                enterText(_confPassTxt, "Admin@123");
                clickOnElement(_restPassBtn);
                reset = false;
            }
            sleep(3);
        }
    }

}
