package Ateroid_Org.Pages;

import Ateroid_Org.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class InquiryPage extends Utils {

    public By _inquiryCard = By.xpath("//*[@id=\"app\"]/section[2]/div[1]/div/section/div/div[1]/a/div/div");
    public By _InquiryAll = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/ul/li[1]/a");
    public By _InquiryPending = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/ul/li[2]/a");
    public By _InquiryCompleted = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/ul/li[3]/a");
    public By _InquiryPaused = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/ul/li[4]/a");
    public By _InquirySearch = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[4]/div/div[1]/div[2]/form/div/input");
    public By _ClickSearch = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[4]/div/div[1]/div[2]/form/div/div/button");
    public By _CreateInquiry = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div[1]/div/div[1]/div[1]/div[2]/a");
    public By _QuickMessage = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/form/div[1]/div/div[1]/div[1]/label");
    public By _QuickMessageField = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/form/div[1]/div/div[1]/div[1]/div/textarea");
    public By _CustomerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[3]/div[1]/div/span/span[1]/span/span[1]/span");
    public By _InquiryType = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/div/span/span[1]/span/ul/li/input");
    public By _AssigneeBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[2]/div[1]/div/div[1]/div/span/span[1]/span/span[1]");
    public By _SearchCustomerName = By.xpath("/html/body/span/span/span[1]/input");
    public By _SelectCustomer = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _SelectInquiryType = By.xpath("/html/body/span/span/span/ul/li");
    public By _SearchAssignee = By.xpath("/html/body/span/span/span[1]/input");
    public By _SelectAssignee = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _AddDueDate = By.xpath("//*[@id=\"due_date\"]");
    public By _AddRemark1 = By.xpath("//*[@id=\"has-many-inquiry_assignee\"]/div[1]/div/div[3]/div/textarea");
    public By _AssigneeBox2 = By.xpath("//div[@id='has-many-inquiry_assignee']/div/div[2]/div/div/span/span/span/span");
    public By _SearchAssignee2 = By.xpath("/html/body/span/span/span[1]/input");
    public By _SelectAssignee2 = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _AddDueDate2 = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/input");
    public By _AddRemark2 = By.xpath("//div[@id='has-many-inquiry_assignee']/div/div[2]/div[3]/div/textarea");
    public By _SaveInquiry = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/form/div[2]/div[2]/div/button");
    public By _BackButton = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/div/div/div[2]");
    public By _ListButton = By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/div/div/div[1]");
    public By _statusGet = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[2]/div[4]/div/span/span[1]/span/span[1]");
    public By _InquiryId = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[1]/td[1]");
    public By _EditInquiry = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[1]/td[9]/a[1]/i");
    public By _statusList = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[2]/div[4]/div/span/span[1]/span");
    public By _PendingStatus = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public By _ClearInquiryType = By.cssSelector("#edit_inquiry > div.box-body > div > div:nth-child(2) > div:nth-child(6) > div > span > span.selection > span > ul > li.select2-selection__choice > span");
    public By _UpdateInquiryType = By.xpath("/html/body/span/span/span/ul/li");
    public By _updateInquiry = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/form/div[2]/div[2]/div/button");
    public By _RedirectionToList = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/div/div/div[1]/a");
    public By _QuickMessage2 = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[1]/div[3]/div/textarea");
    public By _SelectCustomerName2 = By.xpath("//*[@id=\"select2-customer_id-container\"]/span");
    public By _AttachmentLabel2 = By.xpath("//*[@id=\"edit_inquiry\"]/div[1]/div/div[2]/div[6]/label");
    public By _RemarkReason2 = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[2]/div[5]/div/textarea");
    public By _AddAttachment2 = By.xpath("/html/body/div/div/div/section[2]/div/div/section[1]/div/div/div/form/div[1]/div/div[1]/div[4]/div/div/div/input");
    public By _SelectANewAssignee = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/ul/li[2]/a");
    public By _SelectAssigneeBox = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/div/div[2]/form/div[1]/span");
    public By _SelectDueDate = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/ul/li[2]/a");
    public By _AddAssigneeRemark = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/div/div[2]/form/div[3]/textarea");
    public By _SaveAssignee = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/div/div[2]/form/div[4]/button");
    public By _ChangeAssigneeStatusToIn_progress = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/div/div[1]/ul/li[2]/a/div[1]/span[1]");
    public By _ChangeAssigneeStatusToComplete = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/div/div[1]/ul/li[2]/a/div[1]/span[1]");
    public By _DeleteAssignee = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/div/div[1]/ul/li[2]/a/div[1]/span");
    public By _AcceptConfirmationForDelete = By.cssSelector("body > div.swal2-container.swal2-center.swal2-fade.swal2-shown > div > div.swal2-actions > button.swal2-confirm.swal2-styled");
    public By _SearchAssigneeInSearchBox = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/div/div[1]/form/div/input");
    public By _SearchIconForSearchAssignee = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/div/div[1]/form/div/span/button");
    public By _AddAssigneeFollow = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[1]/div/ul/li[1]/a");
    public By _AddAssigneeFollowUp = By.xpath("//*[@id=\"table_0\"]/div/div/div[1]/div/div/a");
    public By _SelectFollowUpLevelBox = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[2]/div[1]/form/div/div[1]/div/select");
    public By _SelectFollowUpLevel = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[2]/div[1]/form/div/div[1]/div/select/option[2]");
    public By _AddFolloUpRemark = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[2]/div[1]/form/div/div[2]/div/textarea");
    public By _UpdateFollowup = By.xpath("/html/body/div/div/div/section[2]/div/div/section[2]/div/div/div/div[2]/div[1]/form/div/div[4]/div/button[1]");
    public By _DeleteInquiry = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[1]/td[9]/a[2]");
    public By _ConfirmDelete = By.xpath("/html/body/div[2]/div/div[3]/button[1]");


    public String currentStatus = null;

    public void InquiryFilters() {
        clickOnElement(_inquiryCard);
        isElementpresent(_InquiryAll);
        isElementpresent(_InquiryPending);
        clickOnElement(_InquiryPending);
        isElementpresent(_InquiryCompleted);
        clickOnElement(_InquiryCompleted);
        isElementpresent(_InquiryPaused);
        clickOnElement(_InquiryPaused);
    }

    public void InquirySearch(String Search) {
        isElementpresent(_InquirySearch);
        clickOnElement(_InquirySearch);
        enterText(_InquirySearch, Search);
        clickOnElement(_ClickSearch);
    }

    public void createInquiry() {
        isElementpresent(_CreateInquiry);
        clickOnElement(_CreateInquiry);
        clickOnElement(_BackButton);
        isElementpresent(_CreateInquiry);
        clickOnElement(_CreateInquiry);
        sleep(1);
        clickOnElement(_ListButton);
        sleep(1);
        isElementpresent(_CreateInquiry);
        clickOnElement(_CreateInquiry);
        isElementpresent(_QuickMessage);
        clickOnElement(_QuickMessage);
        clickOnElement(_QuickMessageField);
        enterText(_QuickMessageField, "Quick Message");
        isElementpresent(_CustomerBox);
        clickOnElement(_CustomerBox);
        sleep(1);
        enterText(_SearchCustomerName, " ");
        sleep(1);
        clickOnElement(_SelectCustomer);
        clickOnElement(_InquiryType);
        clickOnElement(_SelectInquiryType);
        clickOnElement(By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/label"));
        UploadFile(By.xpath(".//input[@type='file']"), By.xpath("//*[@id=\"app\"]/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/div/div/div[1]"));
        isElementpresent(_AssigneeBox);
        clickOnElement(_AssigneeBox);
        enterText(_SearchAssignee, " ");
        sleep(1);
        clickOnElement(_SelectAssignee);
        sleep(1);
        clickOnElement(_AddDueDate);
        clickOnElement(By.cssSelector(".today"));
        enterText(_AddRemark1, "Inquiry reason1");
        isElementpresent(By.cssSelector(".remove"));
        clickOnElement(By.cssSelector(".remove"));
        clickOnElement(By.cssSelector(".add"));
        clickOnElement(_AssigneeBox2);
        enterText(_SearchAssignee2, " ");
        sleep(1);
        clickOnElement(_SelectAssignee2);
        sleep(1);
        clickOnElement(_AddDueDate2);
        clickOnElement(By.cssSelector(".today"));
        enterText(_AddRemark2, "Inquiry reason2");
        sleep(1);
        clickOnElement(_SaveInquiry);
        sleep(1);
    }

    public void EditPendingInquiry(String EditInquiries) {
        isElementpresent(_InquiryId);
        By EditInquiry = By.xpath(EditInquiries + "/a[1]");
        clickOnElement(EditInquiry);
        enterText(_QuickMessage2, "Quick Message Edit");
        clickOnElement(_SelectCustomerName2);
        enterText(_SearchCustomerName, " ");
        sleep(1);
        clickOnElement(_SelectCustomer);
        clickOnElement(_statusList);
        getStatusOption();
        clickOnElement(_PendingStatus);
        clickOnElement(_ClearInquiryType);
        clickOnElement(_UpdateInquiryType);
        clickOnElement(_AttachmentLabel2);
        UploadFile(By.xpath(".//input[@type='file']"), _AddAttachment2);
        sleep(1);
        clickOnElement(_updateInquiry);
        System.out.println("Inquiry updated successfully");
        sleep(2);
        clickOnElement(_SelectANewAssignee);
        clickOnElement(_SelectAssigneeBox);
        sleep(1);
        enterText(_SearchAssignee2, " ");
        sleep(2);
        clickOnElement(_SelectAssignee2);
        sleep(1);
        clickOnElement(_SelectDueDate);
        clickOnElement(By.cssSelector("#due_date > input"));
        clickOnElement(By.cssSelector(".today"));
        enterText(_AddAssigneeRemark, "New assignee remark");
        clickOnElement(_SaveAssignee);
        sleep(1);
        clickOnElement(_ChangeAssigneeStatusToIn_progress);
        sleep(2);
        clickOnElement(_ChangeAssigneeStatusToComplete);
        sleep(2);
        clickOnElement(_DeleteAssignee);
        sleep(1);
        clickOnElement(_AcceptConfirmationForDelete);
        sleep(1);
        enterText(_SearchAssigneeInSearchBox, "admin");
        sleep(1);
        clickOnElement(_SearchIconForSearchAssignee);
        sleep(1);
        clickOnElement(_AddAssigneeFollow);
        clickOnElement(_AddAssigneeFollowUp);
        clickOnElement(_SelectFollowUpLevelBox);
        clickOnElement(_SelectFollowUpLevel);
        enterText(_AddFolloUpRemark, "Follow up check");
        clickOnElement(_UpdateFollowup);
        System.out.println("Completed");
        sleep(2);
    }

    public void EditPausedInquiry(String EditInquiries) {
        isElementpresent(_InquiryId);
        By EditInquiry = By.xpath(EditInquiries + "/a[1]");
        clickOnElement(EditInquiry);
        clickOnElement(_statusList);
        getStatusOption();
        enterText(_RemarkReason2, "Reason Edit 2");
        clickOnElement(_updateInquiry);
        System.out.println("Inquiry updated successfully");
    }

    public String getStatus() {
        clickOnElement(_EditInquiry);
        currentStatus = get_Text(_statusGet);
        System.out.println("currentStat : " + currentStatus);
        return currentStatus;
    }

    public String getStatusOption() {
        List<String> option = new ArrayList<>();
        List<WebElement> options = driver.findElements(By.xpath("/html/body/span/span/span[2]"));
        for (WebElement o : options) {
            option.add(o.getText());
        }
        StringJoiner joiner = new StringJoiner(",");
        for (int i = 0; i < option.size(); i++) {
            joiner.add(option.get(i));
        }
        sleep(2);
        return joiner.toString();
    }


    public void InquiryEdit(String st1, String st2) {
        int i = 0;
        String ST = null;
        boolean hasPausedChecked = false;
        boolean hasPendingChecked = false;
        clickOnElement(_RedirectionToList);
        List<WebElement> Inquiries = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));

        for (i = 1; i <= Inquiries.size(); i++) {
            String EditInquiries = "/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[9]";
            ST = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[7]/span")).getText();
            if (ST.equals(st2) && !hasPausedChecked) {
                System.out.println("Paused");
                EditPausedInquiry(EditInquiries);
                clickOnElement(_RedirectionToList);
                hasPausedChecked = true;
            } else if (st1.contains(ST) && !hasPendingChecked) {
                System.out.println("Pending");
                EditPendingInquiry(EditInquiries);
                clickOnElement(_RedirectionToList);
                hasPendingChecked = true;
            }
            if (hasPendingChecked && hasPausedChecked) {
                return;
            }
        }

    }

    public void DeleteInquiry() {
        clickOnElement(_inquiryCard);
        clickOnElement(_DeleteInquiry);
        sleep(1);
        clickOnElement(_ConfirmDelete);
        sleep(1);
    }

}
