package Ateroid_Org.Pages;

import Ateroid_Org.Utils;
import org.openqa.selenium.By;

public class DashBoardPage extends Utils {

    public By _inquiryCard = By.xpath("//*[@id=\"app\"]/section[2]/div[1]/div/section/div/div[1]/a/div/div");
    public By _Inward_Outward_Card = By.xpath("//*[@id=\"app\"]/section[2]/div[1]/div/section/div/div[2]/a/div/div/h3[2]");
    public By _ServiceCall_Card = By.xpath("//*[@id=\"app\"]/section[2]/div[1]/div/section/div/div[3]/a/div/div");
    public By _Dashboard = By.xpath("//*[@id=\"side-/\"]");
    public By _CallCoordinator = By.xpath("//*[@id=\"side-call-coordinators\"]");
    public By _Inquiry = By.xpath("//*[@id=\"side-inquiries\"]");
    public By _InOutward = By.xpath("//*[@id=\"side-inwards\"]");
    public By _ServiceCall = By.xpath("//*[@id=\"side-service-calls\"]");
    public By _Quotation = By.xpath("//*[@id=\"side-quotations\"]");
    public By _Customer = By.xpath("//*[@id=\"side-locals\"]");
    public By _Master = By.xpath("/html/body/div/aside/div/section/ul/li[9]");
    public By _Admin = By.xpath("/html/body/div/aside/div/section/ul/li[10]");


    public void DashboardCard() {
        isElementpresent(_inquiryCard);
        isElementpresent(_Inward_Outward_Card);
        isElementpresent(_ServiceCall_Card);
    }

    public void CardCheck(String Module) {
        switch (Module) {
            case "Inquiry":
                clickOnElement(_inquiryCard);
                System.out.println("Redirected to Inquiry module successfully.");
                break;

            case "InOutward":
                clickOnElement(_Inward_Outward_Card);
                System.out.println("Redirected to InwardOutward module successfully.");
                break;

            case "ServiceCall":
                clickOnElement(_ServiceCall_Card);
                System.out.println("Redirected to ServiceCall module successfully.");
                break;

            default:
                System.out.println("Card Redirection Failed.");
        }


    }

    public void SideMenu(String Module) {
        switch (Module) {
            case "Dashboard":
                clickOnElement(_Dashboard);
                System.out.println("Redirected to Dashboard module successfully.");
                break;

            case "CallCoordinator":
                clickOnElement(_CallCoordinator);
                System.out.println("Redirected to InwardOutward module successfully.");
                break;

            case "Inquiry":
                clickOnElement(_Inquiry);
                System.out.println("Redirected to Inquiry module successfully.");
                break;

            case "InOutward":
                clickOnElement(_InOutward);
                System.out.println("Redirected to InOutward module successfully.");
                break;

            case "ServiceCall":
                clickOnElement(_ServiceCall);
                System.out.println("Redirected to ServiceCall module successfully.");
                break;

            case "Quotation":
                clickOnElement(_Quotation);
                System.out.println("Redirected to Quotation module successfully.");
                break;

            case "Customer":
                clickOnElement(_Customer);
                System.out.println("Redirected to Customer module successfully.");
                break;

            case "Master":
                clickOnElement(_Master);
                System.out.println("Redirected to Master module successfully.");
                break;

            case "Admin":
                clickOnElement(_Admin);
                System.out.println("Redirected to Admin module successfully.");
                break;

            default:
                System.out.println("SideMenu Redirection Failed.");
        }


    }

}
