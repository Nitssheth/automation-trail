package Ateroid_Org.Pages;

import Ateroid_Org.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ServiceCallPage extends Utils {

    public static By _list = By.xpath("/html/body/div/aside/div/section/ul/li[6]/a");
    public static By _allFilter = By.xpath("/html/body/div/div/div/section[2]/div/div/div/ul/li[1]/a");
    public static By _Open = By.xpath("/html/body/div/div/div/section[2]/div/div/div/ul/li[2]/a");
    public static By _inProgress = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/ul/li[3]/a");
    public static By _close = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/ul/li[4]/a");
    public static By _Search = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[4]/div/div[1]/div[2]/form/div/input");
    public static By _SearchButton = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[4]/div/div[1]/div[2]/form/div/div/button/i");
    public static By _Create = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[1]/div[1]/div[2]/a");
    public static By _backToList = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div/div[1]/a");
    public static By _Back = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/div/div/div[2]/a");
    public static By _SelectCustomerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[3]/div[1]/div/span/span[1]/span/span[1]");
    public static By _SearchCustomer = By.xpath("/html/body/span/span/span[1]/input");
    public static By _SelectCustomer = By.xpath("/html/body/span/span/span[2]/ul/li[2]");
    public static By _SelectContractBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[5]/div/span/span[1]/span/span[1]/span");
    public static By _SearchContract = By.xpath("/html/body/span/span/span[1]/input");
    public static By _SelectContract = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public static By _MessageGiven = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/div/div/input");
    public static By  _SelectTakenByBox= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/div/span/span[1]/span/span[1]");
    public static By  _SearchTakenBy= By.xpath("/html/body/span/span/span[1]/input");
    public static By  _SelectTakenBy= By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public static By  _SelectEngineerBox= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[8]/div/span/span[1]/span/span[1]");
    public static By  _SearchEngineer= By.xpath("/html/body/span/span/span[1]/input");
    public static By  _SelectEngineer= By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public static By  _SelectOpenDate= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[9]/div/div/input");
    public static By  _Amount= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[10]/div/div/input");
    public static By  _Location= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[11]/div/span[2]/label/div/ins");
    public static By  _ComplaintNature= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[12]/div/textarea");
    public static By  _Remark= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[13]/div/textarea");
    public static By  _Attachment= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[14]/div/div/div[4]/div[2]/div/input");
    public static By  _SelectBrandBox= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[1]/div/span/span[1]/span/span[1]");
    public static By  _SearchBrand= By.xpath("/html/body/span/span/span[1]/input");
    public static By _SelectBrand = By.xpath("/html/body/span/span/span[2]/ul/li");
    public static By _SelectProductBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[2]/div/span/span[1]/span/span[1]");
    public static By _SearchProduct = By.xpath("/html/body/span/span/span[1]/input");
    public static By _SelectProduct = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public static By _SelectSubProductBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div[1]/div[3]/div[1]/div/span/span[1]/span/span[1]/span");
    public static By _SearchSubProduct = By.xpath("/html/body/span/span/span[1]/input");
    public static By _SelectSubProduct = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public static By _SubProductRemark = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div[1]/div[3]/div[2]/div/div/input");
    public static By _AddNewProduct = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div[2]/div/div");
    public static By _RemoveNewProduct = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div[1]/div[2]/div[3]/div/div");
    public static By _SelectProblemTypeBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public static By _SearchProblemType = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public static By _SelectProblemType = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public static By _CreateServiceCall = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[2]/div[2]/div/button");
    public static By _RandomClick = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/label");
    public static By _ServiceCallCode = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[1]/div/div/input");
    public static By _EditCustomerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[3]/div/span/span[1]/span/span[1]");
    public static By _EditCustomerSearch = By.xpath("/html/body/span/span/span[1]/input");
    public static By _EditCustomerSelect = By.xpath("/html/body/span/span/span[2]/ul/li[2]");
    public static By _EditContractBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[4]/div/span/span[1]/span/span[1]");
    public static By _EditContractSearch = By.xpath("/html/body/span/span/span[1]/input");
    public static By _EditContractSelect = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public static By _EditMessageGiven = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[5]/div/div/input");
    public static By _EditMessageTakenBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/div/span/span[1]/span/span[1]");
    public static By _EditMessageTakenSearch = By.xpath("/html/body/span/span/span[1]/input");
    public static By _EditMessageTakenSelect = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public static By _EditEngineerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/div/span/span[1]/span/span[1]");
    public static By _EditEngineerSearch = By.xpath("/html/body/span/span/span[1]/input");
    public static By _EditEngineerSelect = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public static By _EditSelectOpenDate = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[8]/div/div/input");
    public static By _EditAmount = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[9]/div/div/input");
    public static By _EditLocation = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[10]/div/span[1]/label/div/ins");
    public static By _EditComplaintNature = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[11]/div/textarea");
    public static By _EditRemark = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[12]/div/textarea");
    public static By _EditSelectBrandBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[1]/div/span/span[1]/span/span[1]");
    public static By _EditSearchBrand = By.xpath("/html/body/span/span/span[1]/input");
    public static By _EditSelectBrand = By.xpath("/html/body/span/span/span[2]/ul/li");
    public static By _EditSelectProductBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[2]/div/span/span[1]/span/span[1]");
    public static By _EditSelectSubProductBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div[1]/div/div[1]/div/span/span[1]/span/span[1]");
    public static By _EditSearchSubProduct = By.xpath("/html/body/span/span/span[1]/input");
    public static By _EditSelectSubProduct = By.xpath("/html/body/span/span/span[2]/ul/li");
    public static By _EditSubProductRemark = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div[1]/div/div[2]/div/div/input");
    public static By _EditAddNewProduct = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div[2]/div/div/i");
    public static By _EditRemoveNewProduct = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div[1]/div[2]/div[3]/div/div");
    public static By _EditSelectProblemTypeBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public static By _EditSearchProblemType = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[5]/div/span/span[1]/span/ul/li/input");
    public static By _EditSelectProblemType = By.xpath("/html/body/span/span/span/ul/li[1]/ul/li[2]");
    public static By _EditStatusBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/div/span/span[1]/span/span[1]");
    public static By _EditStatus = By.xpath("/html/body/span/span/span[2]/ul/li");
    public static By _Status = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/label");
    public static By _UpdateServiceCall = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[2]/div[2]/div/button");
    public static By _randomClick = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[6]/label");
    public static By _ViewPageDetails = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/section[1]/div/div/div/div[1]/h3");
    public static By _confirmDeleteServiceCall = By.xpath("/html/body/div[2]/div/div[3]/button[1]");
    public static By _VerifyServiceCallCode = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[1]/div/div/input");
    public static By _CloseEngineerBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[4]/div/span/span[1]/span/span[1]");
    public static By _CloseEngineerSearch = By.xpath("/html/body/span/span/span[1]/input");
    public static By _CloseEngineerSelect = By.xpath("/html/body/span/span/span[2]/ul/li[4]");
    public static By _CloseSelectCloseDate = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[6]/div/div/input");
    public static By _CloseAmount = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[7]/div/div/input");
    public static By _CloseKM = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[8]/div/div/input");
    public static By _CloseRemark = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[9]/div/textarea");
    public static By _CloseSuggestion = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[10]/div/textarea");
    public static By _CloseAttachment = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[11]/div/div/div[4]/div[2]/div/input");
    public static By _CloseVerifyAttachment= By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[11]/div/div/div[4]/div[1]/input");
    public static By _CloseBrandBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[1]/div/span/span[1]/span/span[1]");
    public static By _CloseBrandSearch = By.xpath("/html/body/span/span/span[1]/input");
    public static By _CloseBrandSelect = By.xpath("/html/body/span/span/span[2]/ul/li");
    public static By _ClosePointBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[2]/div/span/span[1]/span/ul/li/input");
    public static By _ClosePointSearch = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[2]/div/span/span[1]/span/ul/li/input");
    public static By _ClosePointSelect = By.xpath("/html/body/span/span/span/ul/li[1]");
    public static By _CloseTakenBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[3]/div/span/span[1]/span/span[1]/span");
    public static By _CloseTakenSearch = By.xpath("/html/body/span/span/span[1]/input");
    public static By _CloseTakenSelect = By.xpath("/html/body/span/span/span[2]/ul/li[2]");
    public static By _CloseStatusBox = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[2]/div[4]/div/span/span[1]/span/span[1]");
    public static By _CloseStatusSearch = By.xpath("/html/body/span/span/span[1]/input");
    public static By _CloseStatusSelect = By.xpath("/html/body/span/span/span[2]/ul/li[1]");
    public static By _CloseServiceCall = By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[2]/div[2]/div/button");


    public static void serviceCallList() throws ArrayIndexOutOfBoundsException {
        isElementpresent(_list);
        clickOnElement(_list);
    }

    public static void serviceCallFilters() throws ArrayIndexOutOfBoundsException {
        isElementpresent(_allFilter);
        clickOnElement(_allFilter);
        isElementpresent(_Open);
        clickOnElement(_Open);
        sleep(1);
        isElementpresent(_inProgress);
        clickOnElement(_inProgress);
        sleep(2);
        isElementpresent(_close);
        clickOnElement(_close);

    }

    public static void serviceCallSearch(String search) throws ArrayIndexOutOfBoundsException {

        isElementpresent(_Search);
        clickOnElement(_Search);
        enterText(_Search, search);
        clickOnElement(_SearchButton);
    }

    public static void createServiceCall() throws ArrayIndexOutOfBoundsException {
        isElementpresent(_Create);
        clickOnElement(_Create);
        clickOnElement(_backToList);
        isElementpresent(_Create);
        clickOnElement(_Create);
        clickOnElement(_Back);
        clickOnElement(_Create);
        isElementpresent(_SelectCustomerBox);
        clickOnElement(_SelectCustomerBox);
        enterText(_SearchCustomer, " ");
        clickOnElement(_SelectCustomer);
        clickOnElement(_SelectContractBox);
        enterText(_SearchContract," ");
        clickOnElement(_SelectContract);
        enterText(_MessageGiven,"nishit");
        clickOnElement(_SelectTakenByBox);
        enterText(_SearchTakenBy," ");
        clickOnElement(_SelectTakenBy);
        clickOnElement(_SelectEngineerBox);
        enterText(_SearchEngineer," ");
        clickOnElement(_SelectEngineer);
        clickOnElement(_SelectOpenDate);
        enterText(_Amount, "1253");
        clickOnElement(_Location);
        enterText(_ComplaintNature,"xxxx xxxx xxxx xxxx");
        enterText(_Remark,"xxxx xxxx xxxx xxxx");
        UploadFile(_Attachment,By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[14]/div/div/div[4]/div[1]/input"));
        clickOnElement(_SelectBrandBox);
        enterText(_SearchBrand," ");
        clickOnElement(_SelectBrand);
        clickOnElement(_AddNewProduct);
        clickOnElement(_RemoveNewProduct);
        clickOnElement(_SelectProductBox);
        enterText(_SearchProduct, " ");
        sleep(1);
        clickOnElement(_SelectProduct);
        sleep(1);
        clickOnElement(_SelectSubProductBox);
        enterText(_SearchSubProduct, " ");
        sleep(1);
        clickOnElement(_SelectSubProduct);
        sleep(1);
        enterText(_SubProductRemark, "Sub Product remark");
        clickOnElement(_SelectProblemTypeBox);
        enterText(_SearchProblemType, " ");
        clickOnElement(_SelectProblemType);
        clickOnElement(_RandomClick);
        clickOnElement(_CreateServiceCall);
        sleep(2);
    }
    public static void editServiceCall() throws ArrayIndexOutOfBoundsException {
        int i, j;
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("Edit")) {
                    By _EditServiceCall = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]");
                    isElementpresent(_EditServiceCall);
                    clickOnElement(_EditServiceCall);
                    isElementpresent(_ServiceCallCode);
                    clickOnElement(_EditCustomerBox);
                    enterText(_EditCustomerSearch, " ");
                    clickOnElement(_EditCustomerSelect);
                    clickOnElement(_EditContractBox);
                    enterText(_EditContractSearch, "");
                    clickOnElement(_EditContractSelect);
                    clickOnElement(_EditMessageGiven);
                    clickOnElement(_EditMessageTakenBox);
                    enterText(_EditMessageTakenSearch, " ");
                    clickOnElement(_EditMessageTakenSelect);
                    clickOnElement(_EditEngineerBox);
                    enterText(_EditEngineerSearch, " ");
                    clickOnElement(_EditEngineerSelect);
                    clickOnElement(_EditSelectOpenDate);
                    clickOnElement(By.cssSelector("#open_date"));
                    clickOnElement(By.cssSelector(".today"));
                    clickOnElement(_EditAmount);
                    clickOnElement(_EditLocation);
                    enterText(_EditComplaintNature, "Edit");
                    enterText(_EditRemark, "-Edit");
                    UploadFile(By.xpath(".//input[@type='file']"), By.xpath("/html/body/div[1]/div/div/section[2]/div/div/div/form/div[1]/div/div[1]/div[13]/div/div/div[4]/div[1]/input"));
                    clickOnElement(_EditSelectBrandBox);
                    enterText(_EditSearchBrand, " ");
                    clickOnElement(_EditSelectBrand);
                    isElementpresent(_EditSelectProductBox);
                    clickOnElement(_EditSelectSubProductBox);
                    enterText(_EditSearchSubProduct, "");
                    clickOnElement(_EditSelectSubProduct);
                    clickOnElement(_EditSubProductRemark);
                    clickOnElement(_EditAddNewProduct);
                    sleep(1);
                    clickOnElement(_EditRemoveNewProduct);
                    clickOnElement(_EditSelectProblemTypeBox);
                    enterText(_EditSearchProblemType, " ");
                    clickOnElement(_EditSelectProblemType);
                    clickOnElement(_randomClick);
                    clickOnElement(_EditStatusBox);
                    sleep(1);
                    clickOnElement(_EditStatus);
                    clickOnElement(_Status);
                    clickOnElement(_UpdateServiceCall);
                    break outerLoop;
                }

            }

        }
    }

    public static void viewServiceCall() throws ArrayIndexOutOfBoundsException {
        int i, j;
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("View")) {
                    By _ViewServiceCall = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]");
                    isElementpresent(_ViewServiceCall);
                    clickOnElement(_ViewServiceCall);
                    isElementpresent(_ViewPageDetails);
                    break outerLoop;
                }
            }
        }

    }

    public static void closeServiceCall() throws ArrayIndexOutOfBoundsException {
        int i, j;
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("Close")) {
                    By _ViewServiceCall = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]");
                    isElementpresent(_ViewServiceCall);
                    clickOnElement(_ViewServiceCall);
                    isElementpresent(_VerifyServiceCallCode);
                    clickOnElement(_CloseEngineerBox);
                    enterText(_CloseEngineerSearch," ");
                    clickOnElement(_CloseEngineerSelect);
                    clickOnElement(_CloseSelectCloseDate);
                    enterText(_CloseAmount,"500");
                    enterText(_CloseKM,"20");
                    enterText(_CloseRemark,"Close Remark");
                    enterText(_CloseSuggestion,"Close Suggestion");
                    UploadFile(_CloseAttachment,_CloseVerifyAttachment);
                    clickOnElement(_CloseBrandBox);
                    enterText(_CloseBrandSearch," ");
                    clickOnElement(_CloseBrandSelect);
                    clickOnElement(_ClosePointBox);
                    enterText(_ClosePointSearch," ");
                    clickOnElement(_ClosePointSelect);
                    clickOnElement(_CloseTakenBox);
                    enterText(_CloseTakenSearch," ");
                    clickOnElement(_CloseTakenSelect);
                    clickOnElement(_CloseStatusBox);
                    enterText(_CloseStatusSearch, " ");
                    clickOnElement(_CloseStatusSelect);
                    clickOnElement(_CloseServiceCall);
                    break outerLoop;
                }
            }
        }

    }

    public static void printServiceCall() throws ArrayIndexOutOfBoundsException {
        int i, j;
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("Print")) {
                    By _PrintServiceCall = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]");
                    isElementpresent(_PrintServiceCall);
                    clickOnElement(_PrintServiceCall);
                    String link = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]")).getAttribute("href");
                    System.out.println(link);
                    break outerLoop;
                }
            }
        }
    }

    public static void deleteServiceCall() throws ArrayIndexOutOfBoundsException {
        int i, j;
        List<WebElement> rows = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr"));
        outerLoop:
        for (i = 1; i <= rows.size(); i++) {
            List<WebElement> options = driver.findElements(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[\" + i + \"]/td[11]/a"));
            for (j = 1; j <= options.size(); j++) {
                String titleAttribute = driver.findElement(By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]")).getAttribute("title");
                if (titleAttribute.equals("Delete")) {
                    By _deleteServiceCall = By.xpath("/html/body/div/div/div/section[2]/div/div/div/div/div[1]/div/div[2]/table/tbody/tr[" + i + "]/td[11]/a[" + j + "]");
                    isElementpresent(_deleteServiceCall);
                    clickOnElement(_deleteServiceCall);
                    sleep(2);
                    clickOnElement(_confirmDeleteServiceCall);
                    break outerLoop;
                }
            }

        }

    }

    public static void serviceCallOptions(String Option) throws ArrayIndexOutOfBoundsException {
        switch (Option) {
            case "Edit":
                editServiceCall();
                System.out.println("Update successfully.");
                break;

            case "Delete":
                deleteServiceCall();
                System.out.println("Delete successfully.");
                break;

            case "Print":
                printServiceCall();
                System.out.println("Print successfully.");
                break;

            case "Close":
                closeServiceCall();
                System.out.println("Close successfully.");
                break;


            default:
                viewServiceCall();
                System.out.println("View successfully.");

        }

    }

}
