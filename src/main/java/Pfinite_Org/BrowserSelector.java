package Ateroid_Org;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;


import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class BrowserSelector extends Utils {

    public static String status = "passed";
    public static LoadProps loadProp = new LoadProps();
    public static String downloadPath = "/home/poojan/IdeaProjects/automation-trail/target/screenshots";

    @Before
    public static void setUpBrowser(String browser) throws Exception {
        if (browser.equalsIgnoreCase("Chrome")) {
            System.setProperty("webdriver.chrome.silentOutput", "true");
            WebDriverManager.chromedriver().setup();
            ChromeOptions options = new ChromeOptions();
            HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            chromePrefs.put("download.default_directory", downloadPath);
            chromePrefs.put("download.prompt_for_download", false);
            chromePrefs.put("safebrowsing.enabled", true);
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("allow-insecure-localhost");
            options.setExperimentalOption("prefs", chromePrefs);
            options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
            options.setExperimentalOption("useAutomationExtension", false);
            options.setAcceptInsecureCerts(true);
            options.setCapability("disable-restore-session-state", true);
            options.setCapability("profile.default_content_setting_values.cookies", 2);
            options.addArguments("--safebrowsing-disable-download-protection");
            options.addArguments("--test-type");
            options.addArguments("--incognito");
            options.addArguments("--headless");
            options.addArguments("--disable-extensions");
            options.addArguments("disable-infobars");
            options.addArguments("--deleteAll-Cookies");

            driver = new ChromeDriver(options);
            driver.get("https://sample.pfinite.com/auth/login");
            driver.manage().deleteAllCookies();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        } else if (browser.equalsIgnoreCase("Firefox")) {
            //set path to Edge.exe
            System.setProperty("webdriver.firefox.silentOutput", "true");
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions options = new FirefoxOptions();
            HashMap<String, Object> firefoxPrefs = new HashMap<String, Object>();
            firefoxPrefs.put("profile.default_content_settings.popups", 0);
            firefoxPrefs.put("download.default_directory", downloadPath);
            firefoxPrefs.put("download.prompt_for_download", false);
            firefoxPrefs.put("safebrowsing.enabled", true);
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("allow-insecure-localhost");
            options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            options.setAcceptInsecureCerts(true);
            options.setCapability("disable-restore-session-state", true);
            options.setCapability("profile.default_content_setting_values.cookies", 2);
            options.addArguments("--safebrowsing-disable-download-protection");
            options.addArguments("--test-type");
            options.addArguments("--incognito");
//            options.addArguments("--headless");
            options.addArguments("--disable-extensions");
            options.addArguments("disable-infobars");
            options.addArguments("--deleteAll-Cookies");
            //create Edge instance
            driver = new FirefoxDriver();
            driver.get("https://sample.pfinite.com/auth/login");
            driver.manage().deleteAllCookies();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        } else {
            //If no browser passed throw exception

            throw new Exception("Browser is not correct");
        }
    }

    @After
    public void CloseBrowser(Scenario args) {
        driver.quit();
    }
}
